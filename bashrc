# Source our nice prompt
source ~/.bash/prompt.sh

# make ls command show color output
alias ls="ls -G"
# colorize everything
export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced

# Source the shared resource configuration file
source ~/dotfiles/sharedrc
