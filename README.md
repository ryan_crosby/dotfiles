---
title: dotfiles
description: My dotfiles
author: Ryan Crosby
tags: terminal
created: 2012 Feb 19
modified: 2011 Apr 16

---

dotfiles
========

## My dotfiles

These are my dotfiles, I gained much inspiration from 
[Michael Smalley](http://blog.smalleycreative.com/tutorials/using-git-and-github-to-manage-your-dotfiles/)
. They are a work in progress, as I am only adding what I understand. There are many dotfile repos out
there, but for the most part are filled with adjustments that I never realize. So I am making my own
adding only the customizations I want and/or understand.

VIM is mamanged through vundle.
